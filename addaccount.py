import utils

import tweepy


def get_new_auth():
    config = utils.get_config()
    new_auth = tweepy.OAuthHandler(config['consumer_key'], config['consumer_secret'])
    auth_url = None

    try:
        auth_url = new_auth.get_authorization_url()
    except tweepy.TweepError as exception:
        print("Something went wrong whilst getting the authentication URL." +
              " Most likely you have broken API keys. Aborting.")
        print("Printing error.")
        print(exception)
        exit(-2)

    print("Please open up " + auth_url + " and enter the returned verifier below.")
    exit_loop = False
    while not exit_loop:
        verifier = input("Verifier: ")
        exit_loop = True
        try:
            new_auth.get_access_token(verifier)
        except tweepy.TweepError:
            print("Wrong Verifier.")
            exit_loop = False

    return new_auth


def save_auth_to_config(auth, phrase):
    config = utils.get_config()

    apis = config['accounts']
    serialized_api = {
        'id': len(apis) + 1,
        'information':
            {
                'access_token': auth.access_token,
                'access_secret': auth.access_token_secret,
                'phrase': phrase,
                'position_on_wordlist': 1
            }
    }
    apis.append(serialized_api)

    config['accounts'] = apis
    utils.save_config(config=config, filename="config.yml")

print("First i need to know what the account should pe posting. Use '{WORD}' as a placeholder for the actual word.")
phrase = input("Phrase: ")
print("We now need to authenticate the account. This usually only has to be done once per account.")
auth = get_new_auth()
print("Now i'll save the information to the config for you.")
save_auth_to_config(auth=auth, phrase=phrase)
print("Done. Your account is now added and usable. Your config might look a little bit fucked now." +
      " Blame Python, not me.")
