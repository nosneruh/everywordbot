import tweepy
import time

import utils


def limit_rate(cursor):
    while 1:
        try:
            yield cursor.next()
        except tweepy.RateLimitError:
            print('rate limit exceeded, sleeping 15.25min')
            time.sleep(15 * 61)

for api_id, api in utils.get_apis().items():
    print("Now handling Account #" + str(api_id))
    friends_ids = api.friends_ids()
    followers_ids = api.followers_ids()

    for follower_id in limit_rate(tweepy.Cursor(api.followers_ids).items()):
        if follower_id not in friends_ids:
            print('Following user ' + str(follower_id))
            try:
                api.create_friendship(follower_id)
            except tweepy.TweepError as exc:
                print("Couldn't follow user " + str(follower_id) + ", reason: " + exc.reason)

    for friend_id in limit_rate(tweepy.Cursor(api.friends_ids).items()):
        if friend_id not in followers_ids:
            print('Unfollowing user ' + str(friend_id))
            try:
                api.destroy_friendship(follower_id)
            except tweepy.TweepError as exc:
                print("Couldn't follow user " + str(follower_id) + ", reason: " + exc.reason)