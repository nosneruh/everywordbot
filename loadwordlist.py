import sqlite3

import utils

words = []


def load_words():
    with open(utils.get_config()['wordlist'], 'r') as words_file:
        content = words_file.readlines()
        for line in content:
            if not line.startswith("#") and line[0].isupper():
                word = line.split(" ")[0]
                if word not in words:
                    words.append(word)
        words_return = sorted(set(words))
        print('Loaded ' + str(len(words)) + " words.")
    return words_return


def save_words_to_database(words_list):
    db_connection = sqlite3.connect(utils.get_config()['database'])
    db_cursor = db_connection.cursor()
    print("Adding words to database. This may take a while.")
    # Clear table first
    db_cursor.execute("DELETE FROM words;")
    id_now = 0
    for word in words_list:
        db_cursor.execute(
            "INSERT INTO words VALUES (?, ?);",
            (
                id_now,
                word
            )
        )
        id_now += 1

    db_connection.commit()
    db_connection.close()

save_words_to_database(load_words())