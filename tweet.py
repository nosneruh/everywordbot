import utils
import tweepy

config = utils.get_config()
apis = utils.get_apis()
for api_id, api in utils.get_apis().items():
    print("Now handling account #" + str(api_id))
    information = utils.get_account_information(api_id)
    phrase = information['phrase']
    if information['position_on_wordlist'] == -1:
        word = utils.get_random_word()
    else:
        word_id = utils.get_next_word_id(information['position_on_wordlist'])
        word = utils.get_word(id=word_id)
        information['position_on_wordlist'] = word_id
        utils.update_account_information(api_id, information)
    tweet = phrase.replace("{WORD}", word)

    try:
        api.update_status(tweet)
    except tweepy.TweepError as exc:
        print("Something went wrong whilst updating status. Printing error.")
        print(exc)
