import tweepy
import yaml
import sqlite3
from random import randint

def get_apis():
    config = get_config()
    apis = {}

    if len(config['accounts']) == 0:
        print("There is no account authenticated yet. Authenticate one using the addaccount.py script. Aborting.")
        exit(-1)

    for x in range(0, len(config['accounts'])):
        account = config['accounts'][x]['information']
        if 'consumer_key' in account and 'consumer_secret' in account:
            auth = tweepy.OAuthHandler(consumer_key=account['consumer_key'], consumer_secret=account['consumer_secret'])
        else:
            auth = tweepy.OAuthHandler(consumer_key=config['consumer_key'], consumer_secret=config['consumer_secret'])
        auth.set_access_token(key=account['access_token'], secret=account['access_secret'])
        api = tweepy.API(auth_handler=auth)
        if check_api(api=api):
            apis[config['accounts'][x]['id']] = api
        else:
            print("Couldn't authenticate account " + str(config['accounts'][x]['id']))
    return apis


def update_account_information(id, information):
    config = get_config()
    for account in config['accounts']:
        if account['id'] == id:
            account['information'] = information
    save_config(config, "config.yml")


def get_account_information(id):
    result = None
    config = get_config()
    for account in config['accounts']:
        if account['id'] == id:
            result = account['information']
    return result


def get_word(id):
    db_connection = sqlite3.connect(get_config()['database'])
    db_cursor = db_connection.cursor()
    db_cursor.execute("SELECT * FROM words WHERE id=" + str(id) + ";")
    response = db_cursor.fetchone()
    db_connection.commit()
    db_connection.close()
    return response[1]


def get_next_word_id(current_id):
    db_connection = sqlite3.connect(get_config()['database'])
    db_cursor = db_connection.cursor()
    db_cursor.execute("SELECT id FROM words WHERE id>" + str(current_id) + " LIMIT 1;")
    response = db_cursor.fetchone()
    db_connection.close()
    return response[0]

def get_words_amount():
    db_connection = sqlite3.connect(get_config()['database'])
    db_cursor = db_connection.cursor()
    db_cursor.execute("SELECT COUNT(*) FROM words;")
    response = db_cursor.fetchone()
    db_connection.close()
    return response [0]

def get_random_word():
    word_id = randint(0, get_words_amount())
    return get_word(word_id)

def get_config():
    with open("config.yml", 'r') as stream:
        config = None
        try:
            config = yaml.safe_load(stream=stream)
        except yaml.YAMLError as exc:
            print("Couldn't load config.ymy, aborting.")
            print(exc)
            exit(-1)
    return config


def check_api(api):
    result = True

    try:
        api.home_timeline()
    except tweepy.TweepError:
        result = False

    return result


def save_config(config, filename):
    with open(filename, 'w') as outfile:
        outfile.write(yaml.dump(config, default_flow_style=False))
